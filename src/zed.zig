const std = @import("std");
const config = @import("config");
const File = std.fs.File;

const vt_escape = '\x1b';

pub const RowCol = struct { u16, u16 };

pub const Editor = struct {
    ally: std.mem.Allocator,
    term: Terminal,
    width: u16,
    height: u16,
    cursor: RowCol,
    textbuf: std.ArrayList(u8),

    pub fn init(allocator: std.mem.Allocator, terminal: Terminal) !Editor {
        const height, const width = terminal.size();
        return .{
            .ally = allocator,
            .term = terminal,
            .width = width,
            .height = height,
            .cursor = .{ 0, 0 },
            .textbuf = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn deinit(self: Editor) void {
        self.textbuf.deinit();
    }

    pub fn refresh(self: *Editor) !void {
        try self.appendCommand(.{ .resetmode = .cursor });
        try self.appendCommand(.{ .setpos = .{ 0, 0 } });

        try self.drawRows();

        try self.appendCommand(.{
            .setpos = .{
                self.cursor[0] + 1,
                self.cursor[1] + 1,
            },
        });
        try self.appendCommand(.{ .setmode = .cursor });

        self.term.write(self.textbuf.items);

        self.textbuf.items.len = 0;
    }

    pub fn clear(self: Editor) void {
        self.term.writeCommand(.{ .erase = .screen });
        self.term.writeCommand(.{ .setpos = .{ 0, 0 } });
    }

    fn drawRows(self: *Editor) !void {
        for (0..self.height) |row| {
            if (row == self.height / 3) {
                const splash = std.fmt.comptimePrint("zed {}", .{config.version});
                const padding = (self.width - splash.len) / 2;
                if (padding > 0) {
                    try self.textbuf.append('~');
                    try self.textbuf.appendNTimes(' ', padding - 1);
                }
                try self.textbuf.appendSlice(splash[0..@min(splash.len, self.width)]);
            } else {
                try self.textbuf.append('~');
            }

            try self.appendCommand(.{ .eraseline = .right });

            if (row < self.height - 1) {
                try self.textbuf.appendSlice("\r\n");
            }
        }
    }

    fn appendCommand(self: *Editor, command: Terminal.EscapeSequence) !void {
        try std.fmt.format(self.textbuf.writer(), "{}", .{command});
    }

    pub fn processKeypress(self: *Editor, key: u8) void {
        switch (key) {
            ctrl('q') => {
                self.clear();
                std.os.exit(0);
            },
            'h', 'j', 'k', 'l' => self.moveCursor(key),
            else => {},
        }
    }

    fn moveCursor(self: *Editor, key: u8) void {
        var row, var col = self.cursor;
        switch (key) {
            'h' => col -|= 1,
            'j' => row +|= 1,
            'k' => row -|= 1,
            'l' => col +|= 1,
            else => {},
        }
        self.cursor = .{ row, col };
    }

    inline fn ctrl(key: u8) u8 {
        return key & 0x1f;
    }
};

pub const Terminal = struct {
    const os = std.os;
    const linux = os.linux;

    stdin: File,
    stdout: File,
    saved_ios: os.termios,

    pub fn init(in: File, out: File) Terminal {
        const ios = os.tcgetattr(in.handle) catch @panic("cannot read terminal attributes");
        os.tcsetattr(in.handle, os.TCSA.FLUSH, rawFlags(ios)) catch @panic("cannot set terminal raw mode");
        return .{
            .stdin = in,
            .stdout = out,
            .saved_ios = ios,
        };
    }

    pub fn deinit(self: Terminal) void {
        os.tcsetattr(self.stdin.handle, os.TCSA.FLUSH, self.saved_ios) catch
            @panic("failed to restore terminal attrs");
    }

    fn rawFlags(ios: os.termios) os.termios {
        var raw = ios;

        // 'input' attributes:
        raw.iflag.BRKINT = false; // disable SIGINT from break conditions
        raw.iflag.ICRNL = false; // don't translate <C-m> ('\r') into '\n'
        raw.iflag.INPCK = false; // disable parity checking
        raw.iflag.ISTRIP = false; // don't strip the 8th bit of each byte
        raw.iflag.IXON = false; // disable <C-s> and <C-q> control flow

        // 'output' attributes:
        raw.oflag.OPOST = false; // don't translate '\n' into '\r\n'

        // 'local' attributes:
        raw.lflag.ECHO = false; // don't echo key press
        raw.lflag.ICANON = false; // read input byte-by-byte
        raw.lflag.IEXTEN = false; // disable <C-v> and <C-o> control characters
        raw.lflag.ISIG = false; // disable <C-c> and <C-z> signals
        raw.cflag.CSIZE = linux.CSIZE.CS8; // set character size to 8 bits

        raw.cc[@intFromEnum(linux.V.MIN)] = 0; // read input immediately
        raw.cc[@intFromEnum(linux.V.TIME)] = 1; // wait 1/10 of a second (100ms) for input

        return raw;
    }

    pub fn write(self: Terminal, bytes: []const u8) void {
        const n = self.stdout.write(bytes) catch |err| {
            std.debug.panic("error {}\nwhilst writing bytes: {s}", .{ err, bytes });
        };
        std.debug.assert(n == bytes.len);
    }

    pub fn size(self: Terminal) RowCol {
        var w: linux.winsize = undefined;
        const result = linux.ioctl(self.stdout.handle, linux.T.IOCGWINSZ, @intFromPtr(&w));
        if (result == -1) {
            self.writeCommand(.{ .cursor_right = 999 });
            self.writeCommand(.{ .cursor_down = 999 });
            return self.getCursorPos() catch |err|
                std.debug.panic("failed to get cursor position: {}", .{err});
        }

        std.debug.assert(w.ws_row > 0 and w.ws_col > 0);
        return .{ w.ws_row, w.ws_col };
    }

    fn writeCommand(self: Terminal, comptime command: EscapeSequence) void {
        self.write(std.fmt.comptimePrint("{}", .{command}));
    }

    pub fn getCursorPos(self: Terminal) !RowCol {
        self.writeCommand(.{ .dsr = .position });

        var buf: [32]u8 = undefined;
        const coord_str = (try self.stdin.reader().readUntilDelimiter(&buf, 'R'))[2..];

        var result: [2]u16 = undefined;
        var it = std.mem.splitScalar(u8, coord_str, ';');
        var i: usize = 0;
        while (it.next()) |str| : (i += 1) {
            result[i] = try std.fmt.parseUnsigned(u16, str, 10);
        }

        return .{ .row = result[0], .col = result[1] };
    }

    pub fn readKey(self: *Terminal) !u8 {
        const reader = self.stdin.reader();
        const c = try reader.readByte();
        if (c == vt_escape) {
            var seq: [2]u8 = undefined;

            for (&seq) |*char| {
                char.* = reader.readByte() catch return vt_escape;
            }

            if (seq[0] == '[') {
                return switch (seq[1]) {
                    'A' => 'k',
                    'B' => 'j',
                    'C' => 'l',
                    'D' => 'h',
                    else => vt_escape,
                };
            }
        }
        return c;
    }

    // https://vt100.net/docs/vt100-ug/chapter3.html
    pub const EscapeSequence = union(enum) {
        pub const Feature = enum(u8) { cursor = 25 };

        dsr: enum(u3) { status = 5, position = 6 },
        setmode: Feature,
        resetmode: Feature,
        erase: enum(u2) { above = 0, below = 1, screen = 2 },
        eraseline: enum(u2) { right = 0, left = 1, all = 2 },
        setpos: RowCol,
        cursor_right: usize,
        cursor_down: usize,

        pub fn format(value: EscapeSequence, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            try writer.print("{c}[", .{vt_escape});

            try switch (value) {
                .dsr => |report| writer.print("{}n", .{@intFromEnum(report)}),
                .setmode => |mode| writer.print("?{}h", .{@intFromEnum(mode)}),
                .resetmode => |mode| writer.print("?{}l", .{@intFromEnum(mode)}),
                .erase => |area| writer.print("{}J", .{@intFromEnum(area)}),
                .eraseline => |area| writer.print("{}K", .{@intFromEnum(area)}),
                .setpos => |pos| writer.print("{};{}H", .{ pos[0], pos[1] }),
                .cursor_right => |cols| writer.print("{}C", .{cols}),
                .cursor_down => |rows| writer.print("{}B", .{rows}),
            };
        }
    };
};
