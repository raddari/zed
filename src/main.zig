const std = @import("std");
const zed = @import("zed.zig");

pub fn main() !void {
    const stdin = std.io.getStdIn();
    const stdout = std.io.getStdOut();
    var term = zed.Terminal.init(stdin, stdout);
    defer term.deinit();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const ally = gpa.allocator();

    var editor = try zed.Editor.init(ally, term);
    defer {
        editor.clear();
        editor.deinit();
    }

    while (true) {
        try editor.refresh();

        const c = term.readKey() catch |err| switch (err) {
            error.EndOfStream => continue,
            else => std.debug.panic("failed reading input: {}", .{err}),
        };

        editor.processKeypress(c);
    }
}
